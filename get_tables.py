# импорт библиотек
import os
import cv2
import TableExtractor as te

# импорт для текущего модуля
from os import listdir

# переменные
FOLDER_DIR = "C:/Users/Nikolaj/Desktop/Workshop/405/"
# PATH_TO_IMAGE = "C:/Users/Nikolaj/Desktop/Workshop/405/243478 .jpg"


# table_extractor = te.TableExtractor(PATH_TO_IMAGE, '')
# perspective_corrected_image = table_extractor.execute()
# cv2.imshow("perspective_corrected_image", perspective_corrected_image)


# Формируем список с именами справок
images = [img for img in os.listdir(FOLDER_DIR) if img.endswith(".jpg")]

# применяем функцию ко всем изображениям и сохраняем их в выбранную директорию
for i in range(len(images)):
    table_extractor = te.TableExtractor(image_path=FOLDER_DIR + images[i])
    perspective_corrected_image = table_extractor.execute(image_name=images[i])

# cv2.waitKey(0)
# cv2.destroyAllWindows()
